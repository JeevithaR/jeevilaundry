<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/', function () {
    return view('auth.login');
  });
	Auth::routes();



/**
 * Admin Panel
 */
	Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {

  
		Route::get('/dashboard','Admin\DashboardController@index')->name('admin.dashboard.index');
		Route::group(['prefix'=>'bill'],function()
		{
		Route::resource('invoice','Admin\InvoiceController',['as'=>'admin.bill']);
		});
		Route::group(['prefix'=>'basic'],function()
		{
		Route::resource('category','Admin\CategoryController',['as'=>'admin.basic']);
		Route::resource('categorywise-rate','Admin\CategorywiseRateController',['as'=>'admin.basic'],['only'=>'index','create','store']);
		Route::resource('rate-type','Admin\RateTypeController',['as'=>'admin.basic']);
		});
	
	
		});
	


