
<div class="controls">
    <div class="input-group">
         </span>
                    
        <select multiple="multiple" id="categorylist" name="categorylist[]">
        	@foreach($category_list as $list)
                  <option value="{{$list->id}}" >{{$list->name}}   </option>

                           @endforeach
                    </select>
    </div>
</div>
<div class="controls">
    <div class="input-group">
        <div class="text-left">
            <button type="button" id="ms_select" class="btn btn-info">Select All </button>
            <button type="button" id="ms_deselect" class="btn btn-info">Deselect All </button>
        </div>
    </div>
    <div class="text-right">
        <button type="reset"  class="btn btn-danger">Reset <i class="la la-refresh position-right"></i></button>
        <button type="submit" class="btn btn-success">Submit <i class="la la-thumbs-o-up position-right"></i></button>
    </div>
</div>
<script type="text/javascript">
    $().ready(function() {
        if ($("#categorylist").length > 0) {
            $("#categorylist").multiSelect({
                selectableHeader: "<div id='total_student'> Total number of Students 26 | Unallocated Students </div> <input type='text' class='search-input form-control' autocomplete='off' placeholder=' \"Student Name\"'>",
                selectionHeader: "<div id='deselect_item'>Selected Number of students 0</div><input type='text' class='search-input form-control' autocomplete='off' placeholder=' \"Student Name\"'>",
                afterInit: function(ms) {
                    var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';
                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });
                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
                },
                afterSelect: function() {
                    this.qs1.cache();
                    this.qs2.cache();
                    var select_able = $(".ms-selection .ms-list li:visible").length;
                    $("#deselect_item").html("Selected Number of students " + select_able);
                  $("#total_student").html(" Total number of Students 26 |  Unallocated Students ");
                },
                afterDeselect: function() {
                    this.qs1.cache();
                    this.qs2.cache();
                    var select_able = $(".ms-selection .ms-list li:visible").length;
                    $("#deselect_item").html("Selected Number of students " + select_able);
                     $("#total_student").html(" Total number of Students 26 |  Unallocated Students " );
                }
            });
            $("#ms_select").click(function() {
                $('#categorylist').multiSelect('select_all');
            });
            $("#ms_deselect").click(function() {
                $('#categorylist').multiSelect('deselect_all');
            });
        }
    });
</script>
