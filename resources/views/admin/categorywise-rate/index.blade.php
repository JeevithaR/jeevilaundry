
@extends('layouts.admin.header')

@section('title',"Categorywise Rate")

@section('header-style')
@endsection

@section('content-header')
@component('layouts.admin.components.breadcrumb')
@slot('breadcrumb')
<li> <a href="#">Basic</a> </li> 
<li class="active"> <strong>Categorywise Rate List </strong> </li>
@endslot
@slot('title')
Categorywise Rate List
@endslot
@endcomponent
@endsection


@section('content-body')

<a href="{{route('admin.basic.categorywise-rate.create')}}" > <button type="button" class="btn btn-primary float-right">Add/Edit</button> </a>
 <table class="table table-bordered datatable" id="table-4"> 
 <thead>
										<tr>
										<th  rowspan="2"> SNo</th>
										<th rowspan="2" class="font-weight-bold">Category List</th> 
										<th colspan='{{$rateTypeList->count()}}' class="text-center">Types</th> 
										</tr>
										<tr>
										@foreach($rateTypeList as $type)
											<th> {{$type->name}}</th>
										@endforeach
										</tr>
									
									</thead> 
									<tbody>
										@foreach($categoryList as $key => $list)
										<tr>
										<td> {{++$key}}</td>
										<td class="text-uppercase text-center"> {{$list->name}}</td>
											@foreach($rateTypeList as $type)
													<td> 

													<?php
													$rate =0;
													if(optional($list->categoryRate)->count())
													{
														$categoryRate = optional($list->categoryRate)->where('rate_type_id',$type->id)->first();
														 $rate  =$categoryRate->rate;
													}
													
													?>
													<div class="form-group">
															<div class="col-sm-12"> {{$rate}}
															 </div>
													</div>	  
													</td>
											@endforeach
										</tr>
										@endforeach
									</tbody>
	</table> <br />
 
 <script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
var $table4 = jQuery( "#table-4" );
$table4.DataTable( {
dom: 'Bfrtip',
buttons: [
'copyHtml5',
'excelHtml5',
'csvHtml5',
'pdfHtml5'
]
} );
} );
</script> <br />
@endsection 