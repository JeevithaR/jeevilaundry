
@extends('layouts.admin.header')

@section('title',"Categorywise Rate")

@section('header-style')
@endsection
@section('content-header')
@component('layouts.admin.components.breadcrumb')
@slot('breadcrumb')
<li> <a href="#">Basic</a> </li> 
<li class="active"> <strong>Categorywise Rate</strong> </li>
@endslot
@slot('title')
Add Categorywise Rate
@endslot
@endcomponent
@endsection
@section('content-body')

 <div class="row"> 
	<div class="col-md-12"> 
		<div class="panel panel-primary" data-collapsed="0"> 
@component('layouts.admin.components.header')
@slot('title')
Add Categorywise Rate
@endslot
@slot('action')
	<a href="{{route('admin.basic.categorywise-rate.index')}}" > <button type="button" class="btn btn-primary float-right">Back</button> </a>
@endslot
@endcomponent
@include('layouts.admin.success-error')
    <div class="panel-body"> 
			 	<form role="form" method="post" action="{{route('admin.basic.categorywise-rate.store')}}" class="validate"> 
				 {{csrf_field()}}
						<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered" id="table-4"> 
									<thead>
										<tr>
										<th  rowspan="2"> SNo</th>
										<th rowspan="2" class="font-weight-bold">Category List</th> 
										<th colspan='{{$rateTypeList->count()}}' class="text-center">Types</th> 
										</tr>
										<tr>
										@foreach($rateTypeList as $type)
											<th> {{$type->name}}</th>
										@endforeach
										</tr>
									
									</thead> 
									<tbody>
										@foreach($categoryList as $key => $list)
										<tr>
										<td> {{++$key}}</td>
										<td class="text-uppercase text-center"> {{$list->name}}</td>
											@foreach($rateTypeList as $type)
													<td> 

													<?php
													$rate =0;
													if(optional($list->categoryRate)->count())
													{
														$categoryRate = optional($list->categoryRate)->where('rate_type_id',$type->id)->first();
														 $rate  =$categoryRate->rate;
													}
													
													?>
													<div class="form-group">
															<div class="col-sm-12">  <input type="number" value="{{$rate}}"  class="form-control" name="{{$list->id.'_'.$type->id}}"    value="0" /> </div>
													</div>	  
													</td>
											@endforeach
										</tr>
										@endforeach
									</tbody>
							</table>
							</div>
							
						</div>
						<div class="col-sm-12 ">
								<button type="submit" class="btn btn-success pull-right">Submit</button> <button type="reset" class="btn pull-right ">Reset</button> 
							</div> 
					</form> 
			</div>

 </div>
 @endsection
 @section('footer-script')
 <script type="text/javascript">
 	
 	   $(document).ready(function() { 
  
 </script>

 @endsection




