@extends('layouts.admin.header')

@section('title',"Category")

@section('header-style')
@endsection
@section('content-header')
@component('layouts.admin.components.breadcrumb')
@slot('breadcrumb')
<li> <a href="#">Basic</a> </li> 
<li class="active"> <strong>Category</strong> </li>
@endslot
@slot('title')
Add Category
@endslot
@endcomponent
@endsection
@section('content-body')

 <div class="row"> 
	<div class="col-md-12"> 
		<div class="panel panel-primary" data-collapsed="0"> 
@component('layouts.admin.components.header')
@slot('title')
Add Category
@endslot
@slot('action')
	<a href="{{route('admin.basic.category.index')}}" > <button type="button" class="btn btn-primary float-right">Back</button> </a>
@endslot
@endcomponent
 @include('layouts.admin.success-error')
    <div class="panel-body"> 
			 	<form role="form" method="post" action="{{route('admin.basic.category.store')}}" class="validate"> 
			 		 {{ csrf_field() }}
                 <div class="col-md-12">
					 <div class="form-group">
			 		    <label class="col-sm-2 control-label">Category Name</label> 
			 		    <div class="col-sm-4">  <input type="text"  class="form-control" name="category_name" data-validate="required"    data-message-required="This is Categroy Name for required field."   placeholder="Required Field"  /> </div>
			 		 </div>
			 		
			 		
				<br>
				<br>
				<br>
				<div class="col-md-12">

                     <div class="form-group">  <div class="col-sm-6"></div>   <div class="col-sm-6 pull-left"><button type="submit" class="btn btn-success">Submit</button> <button type="reset" class="btn">Reset</button> </div> 
                     </div>
                     </div>
			 	</form> 
			 </div>
 	    </div>
 	</div>
 </div>
 </div>
 @endsection




