
@extends('layouts.admin.header')

@section('title',"Rate Type")

@section('header-style')
@endsection

@section('content-header')
@component('layouts.admin.components.breadcrumb')
@slot('breadcrumb')
<li> <a href="#">Basic</a> </li> 
<li class="active"> <strong>Rate Type</strong> </li>
@endslot
@slot('title')
 Rate Type List
@endslot
@endcomponent
@endsection


@section('content-body')

<a href="{{route('admin.basic.rate-type.create')}}" > <button type="button" class="btn btn-primary float-right">New Rate Type</button> </a>
 <table class="table table-bordered datatable" id="table-4"> 

	<thead>
	 <tr>
	  <th>SNO</th> 
	  <th>Category Name</th> 
	  <th>Status</th>
	 </tr>
	 </thead> 
	 <tbody>
	 	@foreach($rateType as $count => $value)
	 	 <tr class="odd gradeA">
	  <td>{{++$count}}</td> 
	  <td>{{$value->name}}</td> 
	   <td class="center"> <a href="{{route('admin.basic.rate-type.edit',$value->uuid)}}" target="_blank" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a></td>
	   </tr> 

	 	@endforeach

	   </tbody> 
	   <tfoot> 
	   	<tr>
	   	 <th>SNo</th> 
	   	 <th>Category Name</th>  
	   	 <th>Status</th>
	   	 
	   	</tr> 
	   </tfoot> 
	</table> <br />
 
 <script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
var $table4 = jQuery( "#table-4" );
$table4.DataTable( {
dom: 'Bfrtip',
buttons: [
'copyHtml5',
'excelHtml5',
'csvHtml5',
'pdfHtml5'
]
} );
} );
</script> <br />
@endsection 