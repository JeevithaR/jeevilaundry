


@extends('layouts.admin.header')

@section('title',"Rate Type")

@section('header-style')
@endsection
@section('content-header')
<ol class="breadcrumb bc-3" > <li> <a href="../../dashboard/main/index.html"><i class="fa-home"></i>Home</a> </li> <li> <a href="index.html">Forms</a> </li> <li class="active"> <strong>Basic Elements</strong> </li> </ol> <h2>Rate Type</h2> <br />  
@endsection
@section('content-body')


<table class="table table-bordered table-striped datatable" id="table-2"> <thead> <tr> <th> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </th> <th>Student Name</th> <th>Average Grade</th> <th>Curriculum / Occupation</th> <th>Actions</th> </tr> </thead> <tbody> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Ellen C. Jones</td> <td>7.2</td> <td>Education and development manager</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Randy S. Smith</td> <td>8.7</td> <td>Social and human service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Jennifer J. Jefferson</td> <td>10</td> <td>Maxillofacial surgeon</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Carl D. Kaya</td> <td>9.5</td> <td>Express Merchant Service</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>Lillian J. Earl</td> <td>7.6</td> <td>Social and human service assistant</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> <tr> <td> <div class="checkbox checkbox-replace"> <input type="checkbox" id="chk-1"> </div> </td> <td>April L. Baker <span class="label label-success">New Applicant</span></td> <td>6.8</td> <td>Set and exhibit designer</td> <td> <a href="#" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a> </td> </tr> </tbody> </table> <br /> <a href="javascript: fnClickAddRow();" class="btn btn-primary"> <i class="entypo-plus"></i>
Add Row
</a> <br /> <br /> <h3>Table with Column Filtering</h3> <br /> <script type="text/javascript">
jQuery( document ).ready( function( $ ) {
var $table3 = jQuery("#table-3");
var table3 = $table3.DataTable( {
"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
} );
// Initalize Select Dropdown after DataTables is created
$table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
minimumResultsForSearch: -1
});
// Setup - add a text input to each footer cell
$( '#table-3 tfoot th' ).each( function () {
var title = $('#table-3 thead th').eq( $(this).index() ).text();
$(this).html( '<input type="text" class="form-control" placeholder="Search ' + title + '" />' );
} );
// Apply the search
table3.columns().every( function () {
var that = this;
$( 'input', this.footer() ).on( 'keyup change', function () {
if ( that.search() !== this.value ) {
that
.search( this.value )
.draw();
}
} );
} );
} );
</script> <table class="table table-bordered datatable" id="table-3"> <thead> <tr class="replace-inputs"> <th>Rendering engine</th> <th>Browser</th> <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> </tr> </thead> <tbody> <tr class="odd gradeX"> <td>Trident</td> <td>Internet Explorer 4.0</td> <td>Win 95+</td> <td class="center">4</td> <td class="center">X</td> </tr> <tr class="even gradeC"> <td>Trident</td> <td>Internet Explorer 5.0</td> <td>Win 95+</td> <td class="center">5</td> <td class="center">C</td> </tr> <tr class="odd gradeA"> <td>Trident</td> <td>Internet Explorer 5.5</td> <td>Win 95+</td> <td class="center">5.5</td> <td class="center">A</td> </tr> <tr class="even gradeA"> <td>Trident</td> <td>Internet Explorer 6</td> <td>Win 98+</td> <td class="center">6</td> <td class="center">A</td> </tr> <tr class="odd gradeA"> <td>Trident</td> <td>Internet Explorer 7</td> <td>Win XP SP2+</td> <td class="center">7</td> <td class="center">A</td> </tr> <tr class="even gradeA"> <td>Trident</td> <td>AOL browser (AOL desktop)</td> <td>Win XP</td> <td class="center">6</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 1.0</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 1.5</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 2.0</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 3.0</td> <td>Win 2k+ / OSX.3+</td> <td class="center">1.9</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Camino 1.0</td> <td>OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Camino 1.5</td> <td>OSX.3+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Netscape 7.2</td> <td>Win 95+ / Mac OS 8.6-9.2</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Netscape Browser 8</td> <td>Win 98SE+</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Netscape Navigator 9</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.0</td> <td>Win 95+ / OSX.1+</td> <td class="center">1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.1</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.2</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.2</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.3</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.3</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.4</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.4</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.5</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.5</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.6</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.6</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.7</td> <td>Win 98+ / OSX.1+</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.8</td> <td>Win 98+ / OSX.1+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Seamonkey 1.1</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Epiphany 2.20</td> <td>Gnome</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 1.2</td> <td>OSX.3</td> <td class="center">125.5</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 1.3</td> <td>OSX.3</td> <td class="center">312.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 2.0</td> <td>OSX.4+</td> <td class="center">419.3</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 3.0</td> <td>OSX.4+</td> <td class="center">522.1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>OmniWeb 5.5</td> <td>OSX.4+</td> <td class="center">420</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>iPod Touch / iPhone</td> <td>iPod</td> <td class="center">420.1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>S60</td> <td>S60</td> <td class="center">413</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 7.0</td> <td>Win 95+ / OSX.1+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 7.5</td> <td>Win 95+ / OSX.2+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 8.0</td> <td>Win 95+ / OSX.2+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 8.5</td> <td>Win 95+ / OSX.2+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 9.0</td> <td>Win 95+ / OSX.3+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 9.2</td> <td>Win 88+ / OSX.3+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 9.5</td> <td>Win 88+ / OSX.3+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera for Wii</td> <td>Wii</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Nokia N800</td> <td>N800</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Nintendo DS browser</td> <td>Nintendo DS</td> <td class="center">8.5</td> <td class="center">C/A<sup>1</sup> </td> </tr> <tr class="gradeC"> <td>KHTML</td> <td>Konqureror 3.1</td> <td>KDE 3.1</td> <td class="center">3.1</td> <td class="center">C</td> </tr> <tr class="gradeA"> <td>KHTML</td> <td>Konqureror 3.3</td> <td>KDE 3.3</td> <td class="center">3.3</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>KHTML</td> <td>Konqureror 3.5</td> <td>KDE 3.5</td> <td class="center">3.5</td> <td class="center">A</td> </tr> <tr class="gradeX"> <td>Tasman</td> <td>Internet Explorer 4.5</td> <td>Mac OS 8-9</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeC"> <td>Tasman</td> <td>Internet Explorer 5.1</td> <td>Mac OS 7.6-9</td> <td class="center">1</td> <td class="center">C</td> </tr> <tr class="gradeC"> <td>Tasman</td> <td>Internet Explorer 5.2</td> <td>Mac OS 8-X</td> <td class="center">1</td> <td class="center">C</td> </tr> <tr class="gradeA"> <td>Misc</td> <td>NetFront 3.1</td> <td>Embedded devices</td> <td class="center">-</td> <td class="center">C</td> </tr> <tr class="gradeA"> <td>Misc</td> <td>NetFront 3.4</td> <td>Embedded devices</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeX"> <td>Misc</td> <td>Dillo 0.8</td> <td>Embedded devices</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeX"> <td>Misc</td> <td>Links</td> <td>Text only</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeX"> <td>Misc</td> <td>Lynx</td> <td>Text only</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeC"> <td>Misc</td> <td>IE Mobile</td> <td>Windows Mobile 6</td> <td class="center">-</td> <td class="center">C</td> </tr> <tr class="gradeC"> <td>Misc</td> <td>PSP browser</td> <td>PSP</td> <td class="center">-</td> <td class="center">C</td> </tr> <tr class="gradeU"> <td>Other browsers</td> <td>All others</td> <td>-</td> <td class="center">-</td> <td class="center">U</td> </tr> </tbody> <tfoot> <tr> <th>Rendering engine</th> <th>Browser</th> <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> </tr> </tfoot> </table> <br /> <h3>Exporting Table Data</h3> <br /> <script type="text/javascript">
jQuery( document ).ready( function( $ ) {
var $table4 = jQuery( "#table-4" );
$table4.DataTable( {
dom: 'Bfrtip',
buttons: [
'copyHtml5',
'excelHtml5',
'csvHtml5',
'pdfHtml5'
]
} );
} );
</script> <table class="table table-bordered datatable" id="table-4"> <thead> <tr> <th>Rendering engine</th> <th>Browser</th> <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> </tr> </thead> <tbody> <tr class="odd gradeX"> <td>Trident</td> <td>Internet Explorer 4.0</td> <td>Win 95+</td> <td class="center">4</td> <td class="center">X</td> </tr> <tr class="even gradeC"> <td>Trident</td> <td>Internet Explorer 5.0</td> <td>Win 95+</td> <td class="center">5</td> <td class="center">C</td> </tr> <tr class="odd gradeA"> <td>Trident</td> <td>Internet Explorer 5.5</td> <td>Win 95+</td> <td class="center">5.5</td> <td class="center">A</td> </tr> <tr class="even gradeA"> <td>Trident</td> <td>Internet Explorer 6</td> <td>Win 98+</td> <td class="center">6</td> <td class="center">A</td> </tr> <tr class="odd gradeA"> <td>Trident</td> <td>Internet Explorer 7</td> <td>Win XP SP2+</td> <td class="center">7</td> <td class="center">A</td> </tr> <tr class="even gradeA"> <td>Trident</td> <td>AOL browser (AOL desktop)</td> <td>Win XP</td> <td class="center">6</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 1.0</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 1.5</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 2.0</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Firefox 3.0</td> <td>Win 2k+ / OSX.3+</td> <td class="center">1.9</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Camino 1.0</td> <td>OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Camino 1.5</td> <td>OSX.3+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Netscape 7.2</td> <td>Win 95+ / Mac OS 8.6-9.2</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Netscape Browser 8</td> <td>Win 98SE+</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Netscape Navigator 9</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.0</td> <td>Win 95+ / OSX.1+</td> <td class="center">1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.1</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.2</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.2</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.3</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.3</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.4</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.4</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.5</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.5</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.6</td> <td>Win 95+ / OSX.1+</td> <td class="center">1.6</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.7</td> <td>Win 98+ / OSX.1+</td> <td class="center">1.7</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Mozilla 1.8</td> <td>Win 98+ / OSX.1+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Seamonkey 1.1</td> <td>Win 98+ / OSX.2+</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Gecko</td> <td>Epiphany 2.20</td> <td>Gnome</td> <td class="center">1.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 1.2</td> <td>OSX.3</td> <td class="center">125.5</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 1.3</td> <td>OSX.3</td> <td class="center">312.8</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 2.0</td> <td>OSX.4+</td> <td class="center">419.3</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>Safari 3.0</td> <td>OSX.4+</td> <td class="center">522.1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>OmniWeb 5.5</td> <td>OSX.4+</td> <td class="center">420</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>iPod Touch / iPhone</td> <td>iPod</td> <td class="center">420.1</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Webkit</td> <td>S60</td> <td>S60</td> <td class="center">413</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 7.0</td> <td>Win 95+ / OSX.1+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 7.5</td> <td>Win 95+ / OSX.2+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 8.0</td> <td>Win 95+ / OSX.2+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 8.5</td> <td>Win 95+ / OSX.2+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 9.0</td> <td>Win 95+ / OSX.3+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 9.2</td> <td>Win 88+ / OSX.3+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera 9.5</td> <td>Win 88+ / OSX.3+</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Opera for Wii</td> <td>Wii</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Nokia N800</td> <td>N800</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>Presto</td> <td>Nintendo DS browser</td> <td>Nintendo DS</td> <td class="center">8.5</td> <td class="center">C/A<sup>1</sup> </td> </tr> <tr class="gradeC"> <td>KHTML</td> <td>Konqureror 3.1</td> <td>KDE 3.1</td> <td class="center">3.1</td> <td class="center">C</td> </tr> <tr class="gradeA"> <td>KHTML</td> <td>Konqureror 3.3</td> <td>KDE 3.3</td> <td class="center">3.3</td> <td class="center">A</td> </tr> <tr class="gradeA"> <td>KHTML</td> <td>Konqureror 3.5</td> <td>KDE 3.5</td> <td class="center">3.5</td> <td class="center">A</td> </tr> <tr class="gradeX"> <td>Tasman</td> <td>Internet Explorer 4.5</td> <td>Mac OS 8-9</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeC"> <td>Tasman</td> <td>Internet Explorer 5.1</td> <td>Mac OS 7.6-9</td> <td class="center">1</td> <td class="center">C</td> </tr> <tr class="gradeC"> <td>Tasman</td> <td>Internet Explorer 5.2</td> <td>Mac OS 8-X</td> <td class="center">1</td> <td class="center">C</td> </tr> <tr class="gradeA"> <td>Misc</td> <td>NetFront 3.1</td> <td>Embedded devices</td> <td class="center">-</td> <td class="center">C</td> </tr> <tr class="gradeA"> <td>Misc</td> <td>NetFront 3.4</td> <td>Embedded devices</td> <td class="center">-</td> <td class="center">A</td> </tr> <tr class="gradeX"> <td>Misc</td> <td>Dillo 0.8</td> <td>Embedded devices</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeX"> <td>Misc</td> <td>Links</td> <td>Text only</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeX"> <td>Misc</td> <td>Lynx</td> <td>Text only</td> <td class="center">-</td> <td class="center">X</td> </tr> <tr class="gradeC"> <td>Misc</td> <td>IE Mobile</td> <td>Windows Mobile 6</td> <td class="center">-</td> <td class="center">C</td> </tr> <tr class="gradeC"> <td>Misc</td> <td>PSP browser</td> <td>PSP</td> <td class="center">-</td> <td class="center">C</td> </tr> <tr class="gradeU"> <td>Other browsers</td> <td>All others</td> <td>-</td> <td class="center">-</td> <td class="center">U</td> </tr> </tbody> <tfoot> <tr> <th>Rendering engine</th> <th>Browser</th> <th>Platform(s)</th> <th>Engine version</th> <th>CSS grade</th> </tr> </tfoot> </table> <br /><!-- TS16087032979787: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <!-- Footer -->

 @endsection