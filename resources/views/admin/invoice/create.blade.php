@extends('layouts.admin.header')

@section('title',"Invoice")

@section('header-style')
@endsection
@section('content-header')
@component('layouts.admin.components.breadcrumb')
@slot('breadcrumb')
<li> <a href="#">Basic</a> </li> 
<li class="active"> <strong>Invoice</strong> </li>
@endslot
@slot('title')
Add Invoice
@endslot
@endcomponent
@endsection
@section('content-body')

 <div class="row"> 
	<div class="col-md-12"> 
		<div class="panel panel-primary" data-collapsed="0"> 
@component('layouts.admin.components.header')
@slot('title')
Add Invoice
@endslot
@slot('action')
	<a href="{{route('admin.bill.invoice.index')}}" > <button type="button" class="btn btn-primary float-right">Back</button> </a>
@endslot
@endcomponent
 @include('layouts.admin.success-error')
    <div class="panel-body"> 
			 	<form role="form" method="post" action="{{route('admin.bill.invoice.store')}}" class="validate"> 
			 		 {{ csrf_field() }}
            
				  <div class="row col-md-12">
					<div class="col-md-4">
						<div class="form-group">
							<label class=" control-label">Bill No</label> 
							<input type="text"  class="form-control" name="bill_no"   placeholder="Required Field"  />
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class=" control-label">Customer Name</label> 
							<input type="text"  class="form-control" name="name" data-validate="required"    data-message-required="This is Customer Name for required field."   placeholder="Required Field"  />
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class=" control-label">Bill Date</label> 
							<input type="text"  class="form-control" name="bill_date" data-validate="required"    data-message-required="This is Bill Date for required field."   placeholder="Required Field"  />
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label class=" control-label">Email</label> 
							<input type="text"  class="form-control" name="email" data-validate="required"    data-message-required="This is Email for required field."   placeholder="Required Field"  />
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<label class=" control-label">Mobile</label> 
							<input type="text"  class="form-control" name="mobile_number" data-validate="required"    data-message-required="This is Mobile for required field."   placeholder="Required Field"  />
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class=" control-label">Delivery Date</label> 
							<input type="text"  class="form-control" name="delivery_date" data-validate="required"    data-message-required="This is Bill Date for required field."   placeholder="Required Field"  />
						</div>
					</div>

				 </div>
			 		
			 		
				<br>
				<br>
				<br>
				<div class="col-md-12">

                     <div class="form-group">  <div class="col-sm-6"></div>   <div class="col-sm-6 pull-left"><button type="submit" class="btn btn-success">Submit</button> <button type="reset" class="btn">Reset</button> </div> 
                     </div>
                     </div>
			 	</form> 
			 </div>
 	    </div>
 	</div>
 </div>
 </div>
 @endsection




