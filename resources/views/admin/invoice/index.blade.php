
@extends('layouts.admin.header')

@section('title',"Category")

@section('header-style')
@endsection

@section('content-header')
@component('layouts.admin.components.breadcrumb')
@slot('breadcrumb')
<li> <a href="#">Basic</a> </li> 
<li class="active"> <strong>Invoice</strong> </li>
@endslot
@slot('title')
Invoice List
@endslot
@endcomponent
@endsection


@section('content-body')

<a href="{{route('admin.bill.invoice.create')}}" > <button type="button" class="btn btn-primary float-right">New Category</button> </a>
 <table class="table table-bordered datatable" id="table-4"> 

	<thead>
	 <tr>
	  <th>SNO</th> 
	  <th>Invoice Name</th> 
	  <th>Status</th>
	  <th>Action</th>
	 </tr>
	 </thead> 
	 <tbody>
	
	   </tbody> 
	   <tfoot> 
	   	<tr>
	   	 <th>SNo</th> 
	   	 <th>Category Name</th>  
	   	 <th>Status</th>
	   	 <th>Action</th>
	   	 
	   	</tr> 
	   </tfoot> 
	</table> <br />
 
 <script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
var $table4 = jQuery( "#table-4" );
$table4.DataTable( {
dom: 'Bfrtip',
buttons: [
'copyHtml5',
'excelHtml5',
'csvHtml5',
'pdfHtml5'
]
} );
} );
</script> <br />
@endsection 