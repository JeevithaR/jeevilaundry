<!DOCTYPE html> <html lang="en"> 
<!-- Mirrored from demo.neontheme.com/forms/main/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Dec 2020 06:02:07 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1" /> <meta name="description" content="Neon Admin Panel" /> <meta name="author" content="Laborator.co" /> <link rel="icon" href="/assets/images/favicon.ico"> <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141030632-1"></script> <script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-141030632-1', {"groups":"laborator_analytics","link_attribution":true,"linker":{"accept_incoming":true,"domains":["laborator.co","kaliumtheme.com","oxygentheme.com","neontheme.com","themeforest.net","laborator.ticksy.com"]}});</script>
 <title>@yield('title')</title>

  <link rel="stylesheet" href="{{ asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }}
" id="style-resource-1"> <link rel="stylesheet" href="{{ asset('assets/css/font-icons/entypo/css/entypo.css') }}" id="style-resource-2"> <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3"> <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" id="style-resource-4"> <link rel="stylesheet" href="{{ asset('assets/css/neon-core.css') }}
" id="style-resource-5"> <link rel="stylesheet" href="{{ asset('assets/css/neon-theme.css') }}
" id="style-resource-6"> <link rel="stylesheet" href="{{ asset('assets/css/neon-forms.css') }}" id="style-resource-7"> <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" id="style-resource-8"> <script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}
"></script>
  @yield('header-style')
</head>

 <body class="page-body" data-url="https://demo.neontheme.com"> <!-- TS160870329417455: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates -->
  <div class="page-container"> <!-- TS1608703294702: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
  	<div class="sidebar-menu"> 
  		<div class="sidebar-menu-inner">
  		 <header class="logo-env"> <!-- logo --> 
  		 	<div class="logo"> <a href="/dashboard/main/index.html"> <img src="/assets/images/logo%402x.png" width="120" alt="" /> </a> </div> <!-- logo collapse icon --> 
  		 	<div class="sidebar-collapse"> <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --> <i class="entypo-menu"></i> </a> 
  		 	</div> <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) --> <div class="sidebar-mobile-menu visible-xs"> <a href="#" class="with-animation"><!-- add class "with-animation" to support animation --> <i class="entypo-menu"></i> </a> </div> </header>
 @include('layouts.admin.sidemenu')
</div> </div>
 <div class="main-content">


 <!-- TS16087032946490: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
@include('layouts.admin.top-header')
 @yield('content-header')
 @yield('content-body')


 @include('layouts.admin.footer') 

@yield('footer-includes')
@yield('footer-script')