
 <footer class="main"> <div class="pull-right"> <a href="https://themeforest.net/item/neon-bootstrap-admin-theme/6434477?ref=Laborator" target="_blank"><strong>Purchase this theme for $25</strong></a> </div>
		&copy; 2020 <strong>Neon</strong> Admin Theme by <a href="https://laborator.co/" target="_blank">Laborator</a>
		 </footer>
     <link rel="stylesheet" href=" {{ asset('assets/js/datatables/datatables.css') }}" id="style-resource-1">
      
		 <link rel="stylesheet" href="{{ asset('assets/js/select2/select2-bootstrap.css') }}" id="style-resource-1"> <link rel="stylesheet" href=" {{ asset('assets/js/select2/select2.css') }}" id="style-resource-2"> <link rel="stylesheet" href=" {{ asset('assets/js/selectboxit/jquery.selectBoxIt.css') }}" id="style-resource-3"> <link rel="stylesheet" href="{{ asset('assets/js/daterangepicker/daterangepicker-bs3.css') }}" id="style-resource-4"> <link rel="stylesheet" href=" {{ asset('assets/js/icheck/skins/minimal/_all.css') }}" id="style-resource-5"> <link rel="stylesheet" href="{{ asset('assets/js/icheck/skins/square/_all.css') }}" id="style-resource-6"> <link rel="stylesheet" href="{{ asset('assets/js/icheck/skins/flat/_all.css') }}" id="style-resource-7"> <link rel="stylesheet" href="{{ asset('assets/js/icheck/skins/futurico/futurico.css') }}" id="style-resource-8"> <link rel="stylesheet" href="{{ asset('assets/js/icheck/skins/polaris/polaris.css') }}" id="style-resource-9">
     




<script src="{{ asset('assets/js/gsap/TweenMax.min.js') }}" id="script-resource-1"></script> 
<script src="{{ asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}" id="script-resource-2"></script> <script src="{{ asset('assets/js/bootstrap.js') }}" id="script-resource-3"></script>
<script src="{{ asset('assets/js/joinable.js') }}" id="script-resource-4"></script>
 <script src=" {{ asset('assets/js/resizeable.js') }}" id="script-resource-5"></script> 
 <script src=" {{ asset('assets/js/neon-api.js') }}" id="script-resource-6"></script> 
 <script src=" {{ asset('assets/js/cookies.min.js') }}" id="script-resource-7"></script>
  <script src=" {{ asset('assets/js/datatables/datatables.js') }}" id="script-resource-8"></script>
  <script src="{{ asset('assets/js/select2/select2.min.js') }}" id="script-resource-8"></script> 
     <script src="{{ asset('assets/js/bootstrap-tagsinput.min.js') }}" id="script-resource-9"></script> 
     <script src="{{ asset('assets/js/typeahead.min.js') }}" id="script-resource-10"></script> 

     <script src="{{ asset('assets/js/selectboxit/jquery.selectBoxIt.min.js') }}" id="script-resource-11"></script>

 <script src="{{ asset('assets/js/jquery.validate.min.js') }}" id="script-resource-8"></script>
  <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}" id="script-resource-8"></script> 
  <script src=" {{ asset('assets/js/neon-chat.js') }}" id="script-resource-9"></script> <!-- JavaScripts initializations and stuff -->
   <script src="{{ asset('assets/js/neon-custom.js') }}" id="script-resource-10"></script> <!-- Demo Settings --> 
   <script src="{{ asset('assets/js/neon-demo.js') }}" id="script-resource-11"></script> 
   <script src="{{ asset('assets/js/neon-skins.js') }}" id="script-resource-12"></script> 
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}" id="script-resource-12"></script>
       <script src="{{ asset('assets/js/bootstrap-timepicker.min.js') }}" id="script-resource-13"></script> 
     <script src="{{ asset('assets/js/bootstrap-colorpicker.min.js') }}" id="script-resource-14"></script> 
     <script src="{{ asset('assets/js/moment.min.js') }}" id="script-resource-15"></script> 
     <script src="{{ asset('assets/js/daterangepicker/daterangepicker.js') }}" id="script-resource-16"></script>
    <script src="{{ asset('assets/js/jquery.multi-select.js') }}" id="script-resource-17"></script> 
   <script type="text/javascript">
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-28991003-7']);
 _gaq.push(['_setDomainName', 'demo.neontheme.com']);
 _gaq.push(['_trackPageview']);
 (function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
</script> </body> 
<!-- Mirrored from demo.neontheme.com/forms/main/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Dec 2020 06:02:08 GMT -->
</html>