<div class="sidebar-menu"> <div class="sidebar-menu-inner"> 
	 <ul id="main-menu" class="main-menu" > 
	 	<li> 
	 		<a href="{{route('admin.dashboard.index')}}"><i class="entypo-gauge"></i><span class="title">Dashboard</span></a> 
 	    </li>

 	   <li> <a href="frontend/main/index.html" target="_blank"><i class="entypo-newspaper"></i><span class="title">Category Rate</span></a> </li>
     <li class="has-sub"> <a href="ui/panels/index.html"><i class="entypo-monitor"></i><span class="title">Billing</span></a>
     <ul> 
     	<li> <a href="{{route('admin.bill.invoice.index')}}"><span class="title">Invoice</a> </li>
        <li> <a href="ui/tiles/index.html"><span class="title">Tiles</span></a> </li> 
     </ul>
     </li>
	 <li class="has-sub"> <a href="extra/icons/index.html"><i class="entypo-bag"></i><span class="title">Extra</span><span class="badge badge-info badge-roundless">New Items</span></a> 
      	<ul>
         <li class="has-sub"> <a href="extra/icons/index.html"><span class="title">Icons</span><span class="badge badge-success">3</span></a> 
             <ul> 
             	 <li> <a href="extra/icons-fontawesome/index.html"><span class="title">Font Awesome</span></a> </li> 
             	 <li> <a href="extra/icons-entypo/index.html"><span class="title">Entypo</span></a> </li>
                 <li> <a href="extra/icons-glyphicons/index.html"><span class="title">Glyph Icons</span></a> </li>
              </ul>
          </li> 
         <li> <a href="extra/charts/index.html"><i class="entypo-chart-bar"></i><span class="title">Charts</span></a> </li>
         <li class="has-sub"> <a href="index.html#"><i class="entypo-flow-tree"></i><span class="title">Menu Levels</span></a>
         </li> 
        </ul> 
        <li class="has-sub"> 
 		 <a href="#"><span class="title"> <i class="entypo-layout"></i>Basic</span></a>
	 		<ul class="visible"> 
		 		<li> <a href="{{route('admin.basic.category.index')}}"><span class="title">Category List</span></a> </li> 
				 <li class=" active ">  <a href="{{route('admin.basic.rate-type.index')}}"><span class="title">Rate Type List</span></a> </li>
		 	    <li> <a href="{{route('admin.basic.categorywise-rate.index')}}"><span class="title">Categorywise Rate</span></a> </li>    	
	        </ul>
 		</li>
 		</div> 
 </div> 


