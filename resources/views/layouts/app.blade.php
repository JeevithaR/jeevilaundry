<!DOCTYPE html> <html lang="en"> 
<!-- Mirrored from demo.neontheme.com/extra/login/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Dec 2020 06:02:38 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1" /> <meta name="description" content="Jeevi Laundry" /> 
 <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="assets/images/favicon.ico"> <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141030632-1"></script> <script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-141030632-1', {"groups":"laborator_analytics","link_attribution":true,"linker":{"accept_incoming":true,"domains":["laborator.co","kaliumtheme.com","oxygentheme.com","neontheme.com","themeforest.net","laborator.ticksy.com"]}});</script> <title>Neon | Login</title> <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1"> <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css" id="style-resource-2"> <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3"> <link rel="stylesheet" href="assets/css/bootstrap.css" id="style-resource-4"> <link rel="stylesheet" href="assets/css/neon-core.css" id="style-resource-5"> <link rel="stylesheet" href="assets/css/neon-theme.css" id="style-resource-6"> <link rel="stylesheet" href="assets/css/neon-forms.css" id="style-resource-7"> <link rel="stylesheet" href="assets/css/custom.css" id="style-resource-8"> <script src="assets/js/jquery-1.11.3.min.js"></script> <!--[if lt IE 9]><script src="https://demo.neontheme.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]--> <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --> <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]--> <!-- TS1608703300: Neon - Responsive Admin Template created by Laborator --> </head> <body class="page-body login-page login-form-fall" > <!-- TS16087033007013: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> <script type="text/javascript">

</script> 

  @yield('content')






 <script src="assets/js/gsap/TweenMax.min.js" id="script-resource-1"></script> <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script> <script src="assets/js/bootstrap.js" id="script-resource-3"></script> <script src="assets/js/joinable.js" id="script-resource-4"></script> <script src="assets/js/resizeable.js" id="script-resource-5"></script> <script src="assets/js/neon-api.js" id="script-resource-6"></script> <script src="assets/js/cookies.min.js" id="script-resource-7"></script> <script src="assets/js/jquery.validate.min.js" id="script-resource-8"></script> <script src="assets/js/neon-login.js" id="script-resource-9"></script> <!-- JavaScripts initializations and stuff --> <script src="assets/js/neon-custom.js" id="script-resource-10"></script> <!-- Demo Settings --> <script src="assets/js/neon-demo.js" id="script-resource-11"></script> <script src="assets/js/neon-skins.js" id="script-resource-12"></script> <script type="text/javascript">
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-28991003-7']);
 _gaq.push(['_setDomainName', 'demo.neontheme.com']);
 _gaq.push(['_trackPageview']);
 (function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
</script> </body> 
<!-- Mirrored from demo.neontheme.com/extra/login/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Dec 2020 06:02:38 GMT -->
</html>