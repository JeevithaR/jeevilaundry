<?php

namespace App\Model;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
class RateType extends Model
{  
    //
	 Protected $table='rate_types';
    Protected $id ='id';

    
     protected $fillable = ['name','priority','type','status'];


public static function boot()
{
    parent::boot();
    self::creating(function ($model) {
        $model->uuid = (string) Uuid::generate(4);
    });
} 


  
}
