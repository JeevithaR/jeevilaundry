<?php

namespace App\Model;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	  
    
    //
	 Protected $table='task';
    Protected $id ='id';

    
     protected $fillable = ['title','status','date'];
}
