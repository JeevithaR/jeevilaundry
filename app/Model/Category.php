<?php

namespace App\Model;
use Webpatser\Uuid\Uuid;
use App\Model\CategoryRate;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    Protected $table='category';
    Protected $id ='id';
    protected $fillable = ['name','priority','status'];

   /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
public static function boot()
{
    parent::boot();
    self::creating(function ($model) {
        $model->uuid = (string) Uuid::generate(4);
    });
} 

   

    public function categoryRate()
    {
    return $this->hasMany(CategoryRate::class);
    }
   public function status()
   {
   	if($this->status)
   		$status = "<button type='button' class='btn btn-danger btn-sm '> Inactive</button>" ;
   	else
   		$status = "<button type='button' class='btn btn-success btn-sm '> Active
   	</button>";
   	return $status;
   }
   public function action()
   {
 

   $action =	'<a href="'.route('admin.basic.category.edit',$this->uuid).'" target="_blank" class="btn btn-default btn-sm btn-icon icon-left"> <i class="entypo-pencil"></i>
Edit
</a> <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"> <i class="entypo-cancel"></i>
Delete
</a> <a href="#" class="btn btn-info btn-sm btn-icon icon-left"> <i class="entypo-info"></i>
Profile
</a>';
return $action;           
           
          

   }



}

