<?php

namespace App\Model;

use App\Model\RateType;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class CategoryRate extends Model
{
  protected $table="category_rate";
  Protected $id ='id';
  protected $fillable = ['category_id','rate_type_id','rate','status','priority']; 

  public static function boot()
{
    parent::boot();
    self::creating(function ($model) {
        $model->uuid = (string) Uuid::generate(4);
    });
} 
  public function category()
  {
      return belongsToMany(Category::class,'category_id');
  }

  public function rateType()
  {
      return belongsToMany(RateType::class,'rate_type_id');
  }

}
