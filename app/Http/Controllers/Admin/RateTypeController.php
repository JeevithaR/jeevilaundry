<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RateType;
use Webpatser\Uuid\Uuid;
class RateTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rateType = RateType::get();
        return view('admin.rate-type.index',compact('rateType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.rate-type.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      
        $data =[
            'name'=>$request['name'],
            'type' => $request['type']?$request['type']:1,    
                ];
      RateType::create($data);
      session()->flash('flash_message','Rate Type Created Successfully');
      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
       $rate_type =  RateType::where('uuid',$uuid)->first();
       return view('admin.rate-type.edit',compact('rate_type'));
        
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RateType $rateType)
    {
        //
        $data = [
            'name' => $request['name'],
            'type' => $request['type']?$request['type']:1,
        ];
         $rateType->update($data);
         session()->flash('flash_message','Rate Type Updated Successfully');
      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
