<?php

namespace App\Http\Controllers\Admin;
use App\Model\Category;
use App\Model\RateType;
use App\Model\CategoryRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategorywiseRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $rateTypeList =RateType::where('status',0)->get();
        $categoryList =Category::with(['categoryRate'])->where('status',0)->get();
         return view('admin.categorywise-rate.index',compact('rateTypeList','categoryList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        $rateTypeList =RateType::where('status',0)->get();
         $categoryList =Category::with(['categoryRate'])->where('status',0)->get();
         return view('admin.categorywise-rate.create',compact('rateTypeList','categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rateTypeList =RateType::where('status',0)->get();
        $categoryList =Category::where('status',0)->get();
     

        foreach($categoryList as $list)
        {
            $category_id = $list->id;
            foreach($rateTypeList as $type)
           {
            $type_id = $type->id;
            $rate =0;
            $rate = $request[$category_id."_".$type_id];

            $categoryRate = CategoryRate::where([['rate_type_id',$type_id],['category_id',$category_id]])->first();
            if($categoryRate)
            {
                $categoryRate->update(['rate'=>$rate]);
            }
            else{
                $data = [
                    'rate_type_id' =>$type_id,
                    'category_id' => $category_id,
                    'rate'=>$rate,
                ];
                $categoryRate = CategoryRate::create($data);
            }


           }
           session()->flash('flash_message','Categorywise Rate Add/Edit Successfully');
      return redirect()->back();


        }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\CategoryRate  $categoryRate
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryRate $categoryRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\CategoryRate  $categoryRate
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryRate $categoryRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\CategoryRate  $categoryRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryRate $categoryRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\CategoryRate  $categoryRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryRate $categoryRate)
    {
        //
    }
    public function categoryList()
    {
            $rate_type_id = request('rate_type_id'); 

             $category_list =Category::whereDoesntHave('categoryRateType',function($q) use($rate_type_id)
                {
                    $q->where('rate_type_id',$rate_type_id);
                })->get();

              

         return view('admin.category-rate-type.category-list',compact('category_list'));




    }

}
