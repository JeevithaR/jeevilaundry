<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid')->unique()->nullable();
            $table->string('name',50);
            $table->integer('priority',0)->default(0);
            $table->tinyInteger('status', 0)->comment("0-active,1-inactive")->default(0);
            $table->timestamps();
        });



          Schema::create('academic_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid')->unique()->nullable();
            $table->string('title');
            $table->tinyInteger('status', 0)->comment("0-active,1-inactive")->default(0);
            $table->integer('priority', 0)->default(0);
            $table->timestamps();
        });

          Schema::create('category_rate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid')->unique()->nullable();
            $table->bigInteger('category_id')->nullable()->unsigned();
             $table->foreign('category_id')->references('id')->on('category')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->bigInteger('rate_type_id')->nullable()->unsigned();
            $table->foreign('rate_type_id')->references('id')->on('rate_types')->onDelete('cascade')->onUpdate('cascade');
             $table->bigInteger('academic_year_id')->nullable()->unsigned();
            $table->foreign('academic_year_id')->references('id')->on('academic_years')->onDelete('cascade')->onUpdate('cascade');
            $table->tinyInteger('status', 0)->comment("0-active,1-inactive")->default(0);
            $table->integer('priority', 0)->default(0);
            $table->date('active_date')->nullable();
            $table->timestamps(); 
         });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category;');
    }
}
